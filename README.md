# OneVec

A simple `Vec<T>` wrapper with one-based indices allowing for memory layout optimization, e.g.
`Option<NonZeroUsize>` is the same size as `usize` (8 or 4 bytes on most platforms),
while size of `Option<usize>` is two times bigger than `usize` (due to alignment).
